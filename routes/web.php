<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/details', function () {
    return view('details');
});

Route::get('post', function () {
    return view('my_first_post',[
    	'post' => "Hello World"
    ]);
});

Route::get('posts/{post}', function ($slug) {
	$post = file_get_contents(__DIR__,'/resources/posts/{$slug}.php');
    return view('my_first_post',[
    	'post' => $post
    ]);
});
//Route::get('/users/create', 'App\Http\Controllers\UserController@create');
//Route::get('index','UserController::index');
Route::resource('users','App\Http\Controllers\UserController');